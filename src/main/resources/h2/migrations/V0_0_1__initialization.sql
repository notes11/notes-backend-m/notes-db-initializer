/*
 * Engine: H2
 * Version: 0.0.1
 * Description: Initial database structure and data.
 */

CREATE SCHEMA IF NOT EXISTS `notes_db`;

USE `notes_db`;

CREATE TABLE IF NOT EXISTS `base_info`
(
    `Id`                    INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `Uuid`                  VARCHAR(36)  NOT NULL,
    `Version`               INT UNSIGNED NOT NULL,
    `CreatedBy`             VARCHAR(50)  NOT NULL,
    `CreatedTimestamp`      TIMESTAMP    NOT NULL,
    `ModifiedBy`            VARCHAR(50),
    `ModificationTimeStamp` TIMESTAMP,
    `EntryRelatedToTable`   VARCHAR(50)  NOT NULL,
    CONSTRAINT `UQ_Uuid` UNIQUE (`Uuid`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `user`
(
    `Id`               INT UNSIGNED                NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `UserName`         VARCHAR(50)                 NOT NULL,
    `Password`         VARCHAR(80)                 NOT NULL,
    `FirstName`        VARCHAR(50),
    `LastName`         VARCHAR(50),
    `AddressEmail`     VARCHAR(100)                NOT NULL,
    `ActivationStatus` ENUM ('ACTIVE', 'INACTIVE') NOT NULL DEFAULT 'INACTIVE',
    CONSTRAINT `FK_UserId` FOREIGN KEY (`Id`) REFERENCES `base_info` (`Id`),
    CONSTRAINT `UQ_UserName` UNIQUE (`UserName`),
    CONSTRAINT `UQ_AddressEmail` UNIQUE (`AddressEmail`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `role`
(
    `Id`       INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `RoleName` VARCHAR(10)  NOT NULL,
    CONSTRAINT `FK_RoleId` FOREIGN KEY (`Id`) REFERENCES `base_info` (`Id`),
    CONSTRAINT `UQ_RoleName` UNIQUE (`RoleName`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

SET @adminUuid = uuid();
INSERT INTO `base_info` (`Uuid`, `Version`, `CreatedBy`, `CreatedTimestamp`, `EntryRelatedToTable`)
VALUES (@adminUuid, 1, 'init-script', CURRENT_TIMESTAMP(), 'role');

SET @adminId = (SELECT `id` FROM base_info WHERE `Uuid` = @adminUuid);
INSERT INTO `role`(`Id`, `RoleName`)
VALUES (@adminId, 'ADMIN');


SET @userUuid = uuid();
INSERT INTO `base_info` (`Uuid`, `Version`, `CreatedBy`, `CreatedTimestamp`, `EntryRelatedToTable`)
VALUES (@userUuid, 1, 'init-script', CURRENT_TIMESTAMP(), 'role');

SET @userId = (SELECT `id` FROM base_info WHERE `Uuid` = @userUuid);
INSERT INTO `role`(`Id`, `RoleName`)
VALUES (@userId, 'USER');

CREATE TABLE IF NOT EXISTS `user_roles`
(
    `UserId` INT UNSIGNED NOT NULL,
    `RoleId` INT UNSIGNED NOT NULL,
    PRIMARY KEY (`UserId`, `RoleId`),
    CONSTRAINT `FK_UserRolesUserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`),
    CONSTRAINT `FK_UserRolesRoleId` FOREIGN KEY (`RoleId`) REFERENCES role (`Id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `note`
(
    `Id`      INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `Title`   VARCHAR(255),
    `Note`    TEXT         NOT NULL,
    `OwnerId` INT UNSIGNED NOT NULL,
    CONSTRAINT `FK_NoteId` FOREIGN KEY (`Id`) REFERENCES `base_info` (`Id`),
    CONSTRAINT `FK_OwnerId` FOREIGN KEY (`OwnerId`) REFERENCES `user` (`Id`),
    CONSTRAINT `UQ_Titel` UNIQUE (`Title`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;
