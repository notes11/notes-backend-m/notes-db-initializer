package pl.mm.notes.microservice.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pl.mm.notes.core.model.db.Note;
import pl.mm.notes.core.model.db.Role;
import pl.mm.notes.core.model.db.User;

import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = NotesDbInitializerService.class)
public class NotesDbInitializerServiceTests extends AbstractTestNGSpringContextTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeClass
    public void setUpForTests() {
        jdbcTemplate.execute("USE `notes_db`");
    }

    @Test
    public void testBaseInfoTableInitialization() {
        List<Map<String, Object>> resultList = jdbcTemplate.queryForList("SELECT * FROM `base_info`");
        assertTrue(resultList.size() >= 2);
    }

    @Test
    public void testUserTableInitialization() {
        List<User> userList = jdbcTemplate.query("SELECT * FROM `user`", (rs, rowNum) -> User.builder()
                .id(rs.getLong("Id"))
                .userName(rs.getString("UserName"))
                .build());

        assertEquals(userList.size(), 0);
    }

    @Test
    public void testRoleTableInitialization() {
        List<Role> roleList = jdbcTemplate.query("SELECT * FROM `role`", (rs, rowNum) -> Role.builder()
                .id(rs.getLong("Id"))
                .roleName(rs.getString("RoleName"))
                .build());

        assertEquals(roleList.size(), 2);
        assertEquals(roleList.get(0).getRoleName(), "ADMIN");
        assertEquals(roleList.get(1).getRoleName(), "USER");
    }

    @Test
    public void testUserRolesTableInitialization() {
        List<Map<String, Object>> resultList = jdbcTemplate.queryForList("SELECT * FROM `user_roles`");
        assertNotNull(resultList);
    }

    @Test
    public void testNoteTableInitialization() {
        List<Note> noteList = jdbcTemplate.query("SELECT * FROM `note`", (rs, rowNum) -> Note.builder()
                .id(rs.getLong("Id"))
                .title(rs.getString("Title"))
                .build());
        assertNotNull(noteList);
    }

}
